 
// =================================== ADD PRODUCT ================================================= //
const Product = require("../models/product");



module.exports.addProduct = (data) => {
	if(data.isAdmin){
		let new_product = new Product ({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price,
			
		})

		return new_product.save().then((new_product, error) => {
			if(error){
				return false
			}

			return {
				message: 'New product successfully created!'
			}
		});
	}; 


									// Restriction (Admin only) //
	let message = Promise.resolve('User must me an Admin to Access this Product.')

	return message.then((value) => {
		return value


	});
		
};
// ======================================================================================= // 








// =============== Retrieve / GET ALL PRODUCTS ======== //

module.exports.getAllProducts = (data) => {
    if(data.isAdmin){
	return Product.find({}).then(result => {
   		 return result;
    });
};
		let message = Promise.resolve('User must be Admin to Access this.')

		return message.then((value) => {
			return value
	});		

}

// =================================================== //






// =============== Retrieve / GET ALL ACTIVE PRODUCTS ======== //

module.exports.getAllActiveProducts = (data) => {
    
	return Product.find({isActive: true}).then(result => {
   		 return result;
    });

}

// ==================================================== //








// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //
// ================ Retrieve single Product =================== //


module.exports.retrieveSingleProduct = (data) => {
  if (data.isAdmin) {
    return Product.findById(data.id).then((product) => {
      return product;
    });
  }
  let message = Promise.resolve("User must be Admin to use this feature.");

  return message.then((value) => {
    return value;
  });
};



// ============================================================ //
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //









// ========== UPDATE PRODUCT information (Admin only) ======================================= //

module.exports.updateProducts = (reqParams, reqBody) => {

	// specify the fields/properties of the document to be updated


	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};


		return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((course, error) => {

				if (error) {

					return false;

				} else {

					return {
						message: "Product updated successfully"
					}
				};


		});

};
// ================================================================================================== //









// ===============================ARCHIVE PRODUCT ( Admin Only )============================================================= //

module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		
		isActive : false
	};

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
			if (error) {

				return false;
			} else {
				return {
					message : "Archiving Product successfully"
				}
			};
		});

};
// ================================================================================================================ //








// =============================== UNARCHIVE PRODUCT ( Admin Only ) ============================================================= //

module.exports.unarchiveProduct = (reqParams) => {

	let updateActiveField = {
		
		isActive : true
	};

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
			if (error) {

				return false;
			} else {
				return {
					message : "Unarchiving Product successfully"
				}
			};
		});

};
// ================================================================================================================ //








// ========== Non Admin User Checkout ( Create Order ) ====================== //

// module.exports.userCheckout = (data) => {
//   if (data.isUser) {
//     return Product.findById(data.id).then((product) => {
//       return product;
//     });
//   }
//   let message = Promise.resolve("Your item has been check out.");

//   return message.then((value) => {
//     return value;
//   });
// };





// ============================================================================ //







// Retrieve User Details
// =================== RETRIEVE ACTIVE PRODUCTS ================================== //


module.exports.getAllActive = () => {

	 return Product.find({isActive : true}).then(result => {
	 	return result;
	 }) ;
};
// ====================================================================== //
