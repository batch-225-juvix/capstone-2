// ====== INSTALLER ================= //

const mongoose = require("mongoose");

// ================================== //


// ========== SCHEMA for orders=============== //

const productSchema = new mongoose.Schema({


    orders: [{

            products : [{
                
                productName: String,
                required: [true, "Product name is required"]
            },

            {

                quantity: Number,
                required: [true, "Quantity is required"]

            }],




            totalAmount : {
                type: Number,
                default: "Purchased"
            },


            purchasedOn : {
                type: Date,
                default: new Date()
               
            }


    }]




});

// ============================================================== //





// =========CONSOLE LOG for Orders ===========//

    module.exports = mongoose.model("order", productSchema);

// ======================================================== //